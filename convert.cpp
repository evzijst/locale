#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <locale.h>

using namespace std;

char * format(double d, char *buf) {
  char *loc = setlocale(LC_ALL, NULL);
  setlocale(LC_ALL, "nl_NL.utf-8");
  sprintf(buf, "%.2f", d);
  loc = setlocale(LC_ALL, loc); // restore locale
  return buf;
}

int main(int argc, char** argv) {
  if (argc != 2 || atof(argv[1]) == 0.0) {
    cerr << "Usage: " << argv[0] << " <number>\n";
    return 1;

  } else {
    char formatted_value[255];
    cout << format(atof(argv[1]), formatted_value) << "\n";
    return 0;
  }
}
