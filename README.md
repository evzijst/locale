    :::bash
    $ c++ -o convert convert.cpp
    $ ./convert 10
    10,00
    $ ./convert 15.31
    15,31
    $ ./convert 15.355
    15,36
    $ ./convert 14.3
    14,30
    $ ./convert 14.770
    14,77
